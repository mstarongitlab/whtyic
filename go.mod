module gitlab.com/mstarongitlab/whtyic

go 1.21

toolchain go1.21.5

require (
	github.com/swaywm/go-wlroots v0.0.0-20240106150514-d5477b0e4b27 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
