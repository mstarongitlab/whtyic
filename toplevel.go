// Copyright (c) 2024 mStar
//
// Licensed under the EUPL, Version 1.2
//
// You may not use this work except in compliance with the Licence.
// You should have received a copy of the Licence along with this work. If not, see:
// <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

package main

import "github.com/swaywm/go-wlroots/wlroots"

type TopLevel struct {
	surface     wlroots.XDGSurface
	Mapped      bool
	X           float64
	Y           float64
	xdgTopLevel wlroots.XDGTopLevel
	SceneTree   wlroots.SceneTree
}

func NewTopLevel(surface wlroots.XDGSurface) *TopLevel {
	return &TopLevel{surface: surface}
}

func (v *TopLevel) Surface() wlroots.Surface {
	return v.surface.Surface()
}

func (v *TopLevel) XDGSurface() wlroots.XDGSurface {
	return v.surface
}
